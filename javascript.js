$("#general i .counter").text(" ");

var fnUpdateCount = function() {
  var generallen = $("#general-content input[name='wpmm[]']:checked").length;
  console.log(generallen, $("#general i .counter"));
  if (generallen > 0) {
    $("#general i .counter").text(generallen);
  } else {
    $("#general i .counter").text(" ");
  }
};

$("#general-content input:checkbox").on("change", function() {
  fnUpdateCount();
});

$(".select_all").change(function() {
  var checkthis = $(this);
  var checkboxes = $("#general-content input:checkbox");

  if (checkthis.is(":checked")) {
    checkboxes.prop("checked", true);
  } else {
    checkboxes.prop("checked", false);
  }
  fnUpdateCount();
});

var checkboxValues = JSON.parse(localStorage.getItem('checkboxValues')) || {},
    $checkboxes = $("#general-content :checkbox");

$checkboxes.on("change", function(){
  $checkboxes.each(function(){
    checkboxValues[this.id] = this.checked;
  });
  
  localStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
});

// On page load
$.each(checkboxValues, function(key, value) {
  $("#" + key).prop('checked', value);
});
